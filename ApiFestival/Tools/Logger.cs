﻿using System;
using System.IO;

namespace ApiFestival
{
    public class Logger : IDisposable
    {
        public void Dispose()
        { }

        public void WriteLog(string log, Exception ex)
        {
            using (StreamWriter sw = new StreamWriter("_Log/" + DateTime.Now.ToString("yyyy-MM-dd") + "_log.txt", true))
            {
                sw.WriteLine("*** " + log + " | " + DateTime.Now.ToString("HH:mm:ss") + " ***");
                sw.WriteLine(ex.Message + " - " + ex.InnerException);
                sw.WriteLine(ex.StackTrace);
            }
        }
    }
}
