﻿using Microsoft.AspNetCore.Authorization;

namespace ApiFestival
{
    public class AuthorizeRolesAttribute : AuthorizeAttribute
    {
        public AuthorizeRolesAttribute(params string[] roles) : base()
        {
            Roles = string.Join(",", roles);
        }
    }

    public  class Roles
    {
        public const string Administrateur = "administrateur";
        public const string Client = "client";
    }
}
