﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ApiFestival
{

    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    [Route("[controller]")]
    public class userconnectionController : ControllerBase
    {
        private IuserconnectionService _userconnectionService;

        public userconnectionController(IuserconnectionService userconnectionService)
        {
            _userconnectionService = userconnectionService;
        }

        #region Login

        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody] LoginModel login)
        {

            ReponseAPI reponse = await _userconnectionService.Login(login);
            return StatusCode(reponse.StatusCode, reponse);
        }

        #endregion

        #region Register
        [AllowAnonymous]
        [HttpPost("Register")]
        public async Task<IActionResult> Register([FromBody] Users tiers)
        {
            ReponseAPI reponse;

     
           reponse = await _userconnectionService.Register(tiers);

            return StatusCode(reponse.StatusCode, reponse);
        }
        #endregion



    }
}
