﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;


namespace ApiFestival
{
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    [Route("[controller]")]
    public class FestivalController : ControllerBase
    {
        private IService<Festival> _festivalService;

        public FestivalController(IService<Festival> festivalService)
        {
            _festivalService = festivalService;
        }

        #region Get

        /// <summary>
        /// Récupérer une personne à partir de son id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //[AuthorizeRoles(Roles.Client)]
        [HttpGet("{id:int}")]
        public IActionResult GetById(int id)
        {
            ReponseAPI reponse = _festivalService.GetById(id);
            return StatusCode(reponse.StatusCode, reponse);
        }
        #endregion

        #region Add

        /// <summary>
        /// Ajouter une personne
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
     
        [HttpPost]
        //[AuthorizeRoles(Roles.Administrateur)]
        public IActionResult Add([FromBody] Festival data)
        {
            ReponseAPI reponse = _festivalService.Add(data);
            return StatusCode(reponse.StatusCode, reponse);
        }

        #endregion

        #region Update

        /// <summary>
        /// Modifier une personne
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns
        
        [HttpPut]
        //[AuthorizeRoles(Roles.Administrateur)]
        public ActionResult Update([FromBody] Festival data)
        {
            ReponseAPI reponse = _festivalService.Update(data);
            return StatusCode(reponse.StatusCode, reponse);
        }

        #endregion

        #region GetAllArtist

        [HttpGet("Artistes/{id:int}")]
        public IActionResult GetAllArtistsByFestivalId(int id)
        {
            ReponseAPI reponse = _festivalService.GetAllArtistsByFestivalId(id);
            return StatusCode(reponse.StatusCode, reponse);
        }
        #endregion
    }




}
