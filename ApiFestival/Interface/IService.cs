﻿
using System.Collections.Generic;

namespace ApiFestival
{
    public interface IService<T>
    {
        ReponseAPI GetById(int id);
        

        ReponseAPI Add(T data);
        ReponseAPI Update(T data);
        ReponseAPI Synchro(List<T> liste);
        ReponseAPI GetAllArtistsByFestivalId(int id);
    }
}
