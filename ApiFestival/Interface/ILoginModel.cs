﻿namespace ApiFestival
{
    public interface ILoginModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
    }
}
