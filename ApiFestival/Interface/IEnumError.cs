﻿namespace ApiFestival
{
    public interface IEnumError
    {
        public const string ValueIsNull = "Valeur null";
        public const string ListIsEmpty = "La liste est vide";
        public const string GetByIdError = "Erreur lors de la récupération d'un ";
        public const string GetByFilterError = "Erreur lors de la récupération de plusieurs ";
        public const string AddOneError = "Erreur lors de l'ajout d'une ";
        public const string AddMultipleError = "Erreur lors de l'ajout de plusieurs ";
        public const string UpdateOneError = "Erreur lors de la modification d'un ";
        public const string UpdateMultipleError = "Erreur lors de la modification de plusieurs ";
        public const string SynchroError = "Erreur lors du tri pour la synchronisation";
    }
}
