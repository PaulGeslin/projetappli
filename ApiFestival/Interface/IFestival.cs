﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiFestival
{
    public interface IFestival
    {
        public int Id { get; set; }
        public string Nom{ get; set; }
        public string Rue { get; set; }
        public string Departement { get; set; }
        public string Pays { get; set; }
        public string Ville { get; set; }
    }
    

}
