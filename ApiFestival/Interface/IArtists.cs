﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiFestival
{
    public interface IArtists
    {
        public int Id { get; set; }
        public int IdFestival { get; set; }
        public string Nom{ get; set; }
        public string Description{ get; set; }
        
    }
    

}
