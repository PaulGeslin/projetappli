﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiFestival
{
    public interface IFilterAPI<TFilter>
        where TFilter : IFilter
    {
        public string Take { get; set; }
        public string Skip { get; set; }
        public string Sort { get; set; }

        public List<TFilter> Filter { get; set; }

        public List<string> Children { get; set; }

        public bool OrderDesc { get; set; }
    }

    public interface IFilter
    {
        public string Property { get; set; }
        public List<string> Values { get; set; }
        //public Operators Operator { get; set; }
    }

   /* public enum Operators
    {
        Eq,                 //0
        Neq,                //1
        Contains,           //2
        DoesNotContains,    //3
        StartsWith,         //4
        EndsWith,           //5
        IsNull,             //6
        IsNotNull,          //7
        IsEmpty,            //8
        IsNotEmpty          //9
    }*/

}
