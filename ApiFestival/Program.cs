﻿using System.IO;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using System;

namespace ApiFestival
{
    public class Program
    {
        public static string ParamPath = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()) + "/param.xml");

        public static void Main(string[] args)
        {
            if (!File.Exists(ParamPath))
            {
                var gp = new GlobalParameter();

                gp.ApiUrl = "http://localhost";
                gp.ApiPort = 90;

                

                
            }

            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    var apiPort = 90;

                    

                    webBuilder.UseStartup<Startup>()
                    .UseUrls("http://localhost:" + apiPort)
                    .UseSerilog((context, configuration) =>
                    {
                        configuration
                            .MinimumLevel.Debug()
                            .MinimumLevel.Override("Microsoft", LogEventLevel.Error)
                            .MinimumLevel.Override("System", LogEventLevel.Error)
                            .MinimumLevel.Override("Microsoft.AspNetCore.Authentication", LogEventLevel.Error)
                            .Enrich.FromLogContext()
                            .WriteTo.File("_Log/logError.txt")
                            .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level}] {SourceContext}{NewLine}{Message:lj}{NewLine}{Exception}{NewLine}", theme: AnsiConsoleTheme.Literate);
                    }); ;
                });
    }
}
