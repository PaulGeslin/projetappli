﻿

namespace ApiFestival
{
    public class LoginModel : ILoginModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
    }
}
