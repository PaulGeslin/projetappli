﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiFestival
{
    public class Festival : IFestival
    {
        [Key]
        public int Id { get; set; }

        [Column(TypeName = "varchar(255)")]
        public string Nom { get; set; }

        [Column(TypeName = "nvarchar(255)")]
        public string Rue { get; set; }

        [Column(TypeName = "nvarchar(255)")]
        public string Departement { get; set; }
        
        [Column(TypeName = "nvarchar(255)")]
        public string Pays { get; set; }
        
        [Column(TypeName = "nvarchar(255)")]
        public string Ville { get; set; }


       
    }
}
