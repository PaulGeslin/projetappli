﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiFestival
{
   public class ReponseAPI : IReponseAPI
    {

        public int StatusCode { get; set; }
        public string Message { get; set; }
        public string MessageException { get; set; }
        public string StackTrace { get; set; }
        public object Objet { get; set; }
    }
}
