﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiFestival
{
    public class FilterAPI : IFilterAPI<Filter>
    {
        public string Take { get; set; }
        public string Skip { get; set; }
        public string Sort { get; set; }

        public List<Filter> Filter { get; set; }

        public List<string> Children { get; set; }

        public bool OrderDesc { get; set; }
    }



    public class Filter : IFilter
    {
        public string Property { get; set; }
        public List<string> Values { get; set; }
        public Operators Operator { get; set; }
    }


    
    public enum Operators
    {
        Eq,                 //0
        Neq,                //1
        Contains,           //2
        DoesNotContains,    //3
        StartsWith,         //4
        EndsWith,           //5
        IsNull,             //6
        IsNotNull,          //7
        IsEmpty,            //8
        IsNotEmpty          //9
    }
}
