﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Security.Claims;

namespace ApiFestival
{
    public class Users  : IdentityUser<string>
    {
       
         
         public string Role{ get; set; }
        public new Byte TwoFactorEnabled { get; set; }
        public new Byte PhoneNumberConfirmed { get; set; }
        public new Byte LockoutEnabled { get; set; }
        public new Byte EmailConfirmed { get; set; }



        public class userconnectionEnumError
        {
            public const string EmailAlreadyUse = "Cette adresse mail est déjà utilisé";
            public const string LoginIsNull = "Information de connexion non renseigné";
            public const string RegisterNotAuthorized = "Vous n'avez pas l'autorisation pour créer un compte";
            public const string LoginIncorrect = "Identifiant ou mot de passe incorrect";
            public const string TokenError = "Erreur lors de la création du token"; 
            public const string RoleNotFound = "Le rôle n'existe pas.";
            public const string PasswordFalse = "Le mot de passe est invalide.";
        }


    }
}