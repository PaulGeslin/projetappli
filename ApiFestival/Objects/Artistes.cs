﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiFestival
{
    public class Artistes :  IArtists
    {
       
        public int Id { get; set; }

       
        public int IdFestival { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
    }
}
