﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiFestival
{
    public class GlobalParameter
    {

        public string ApiUrl { get; set; }
        public int ApiPort { get; set; }

        public string PortailUrlHttp { get; set; }
       
    }
}
