﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;


namespace ApiFestival
{
    public class ApiFestivalContext : IdentityDbContext
    {

        public ApiFestivalContext(DbContextOptions<ApiFestivalContext> options) : base(options) { }

        public DbSet<Festival> Festival { get; set; }
        public DbSet<Artistes> Artistes { get; set; }
        public DbSet<Users> Users { get; set; }
      



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
           
            base.OnModelCreating(modelBuilder);
            
        }

    }
}
