﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;


namespace ApiFestival

{
    public interface IuserconnectionService
    {
        Task<ReponseAPI> Login(LoginModel data);
        Task<ReponseAPI> Register(Users data);
    }

    public class userconnectionservice : IuserconnectionService
    {
        private readonly ApiFestivalContext _context;
        private readonly UserManager<Users> _userManager;
        private readonly SignInManager<Users> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IConfiguration _configuration;
        private const string TYPE_DATA = "UtilisateurAPI";



        public userconnectionservice(ApiFestivalContext context,
             UserManager<Users> userManager,
             SignInManager<Users> signInManager,
             RoleManager<IdentityRole> roleManager,
             IConfiguration configuration

             )

        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _configuration = configuration;


        }

        /// <summary>
        /// Récupérer un token de connexion avec un Tier
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<ReponseAPI> Login(LoginModel data)
        {
            ReponseAPI reponse;
            Users user;

            if (data == null || string.IsNullOrWhiteSpace(data.Login) || string.IsNullOrWhiteSpace(data.Password))
            {
                reponse = new ReponseAPI
                {
                    StatusCode = 400,
                    Message = IEnumError.ValueIsNull
                };
                return reponse;
            }

            //Récupérer le user à partir du mail
            try
            {
                user = _context.Users.Where(x => x.Email == data.Login).FirstOrDefault();

                if (user == null)
                {
                    reponse = new ReponseAPI
                    {
                        StatusCode = 500,
                        Message = Users.userconnectionEnumError.LoginIncorrect
                    };
                    return reponse;
                }
                else
                {
                    if (data.Password != user.PasswordHash)
                    {
                        reponse = new ReponseAPI
                        {
                            StatusCode = 500,
                            Message = Users.userconnectionEnumError.PasswordFalse
                        };
                        return reponse;
                    }
                    else {

                        //Task<string> email = GenerateJwtTokenAsync(user);

                        reponse = new ReponseAPI
                        {
                            StatusCode = 200,
                            Message = user.Email
                        };
                        return reponse;

                    }
                    
                }
            }
            catch (Exception ex)
            {
                reponse = new ReponseAPI
                {
                    StatusCode = 500,
                    Message = IEnumError.GetByIdError + TYPE_DATA,
                    MessageException = ex.Message + ex.InnerException
                };
                return reponse;
            }

            //test de la combinaison Password => Email
            
        }

        #region Fonction

        /// <summary>
        /// Récupérer le mail dans le token
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static string GetEmailInToken(string token)
        {
            try
            {
                JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
                JwtSecurityToken jwt = handler.ReadJwtToken(token);
                return jwt.Subject;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        /// <summary>
        /// Générer un token JWT avec un rôle pour la connexion.
        /// </summary>
        /// <param name="user"></param>
        /// <returns>string : le token généré.</returns>
        public async Task<string> GenerateJwtTokenAsync(Users user)
        {

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("Une_GraNde22Clef__P0uur_La_G3neRaTi0n_Ud_T0126kEn_Sin0oN_Y_N_Pa_0k"));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken("http://localhost:90/Issuer",
              "http://localhost:90/Issuer",
              null,
              expires: DateTime.Now.AddDays(1),
              signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
            
        }

        /// <summary>
        /// Inscrire un utilisateur
        /// </summary>
        /// <param name="data"></param>
        /// <param name="token"></param>
        /// <returns>string : le token généré.</returns>
        public async Task<ReponseAPI> Register(Users data)
        {
            ReponseAPI reponse;
            Users user = new Users();

            if (string.IsNullOrWhiteSpace(data.Email) || string.IsNullOrWhiteSpace(data.PasswordHash))
            {
                reponse = new ReponseAPI
                {
                    StatusCode = 400,
                    Message = Users.userconnectionEnumError.LoginIsNull
                };
                return reponse;
            }
            try
            {
                //Rechercher le Users par l'email, si non trouvé, générer un id
                //Si trouvé, erreur "email existe déja"
                user = _context.Users.Where(x => x.Email == data.Email).FirstOrDefault();

                if (user != null)
                {
                    reponse = new ReponseAPI
                    {
                        StatusCode = 400,
                        Message = Users.userconnectionEnumError.EmailAlreadyUse
                    };
                    return reponse;
                }
                else
                {
                    user = new Users();

                        //Créer un ID et lui affecter le rôle client
                        user.Id = Guid.NewGuid().ToString();
                        user.Role = "client";

                        //Affecter les paramètres de la requête
                        user.Email = data.Email;
                        user.UserName = data.UserName;
                        user.PasswordHash = data.PasswordHash;
                        user.NormalizedEmail = data.Email.ToUpper();
                        user.NormalizedUserName = data.UserName.ToUpper();
                        user.SecurityStamp = Guid.NewGuid().ToString();

                        //Ajouter dans la bdd
                        _context.Users.Add(user);
                        _context.SaveChanges();

                        //Vérification de l'insertion en bdd
                        Users Trouve = _context.Users.Where(x => x.Email == user.Email).FirstOrDefault();

                        if (Trouve != null)
                        {
                            reponse = new ReponseAPI
                            {
                                StatusCode = 200,
                                Message = "Inscription réalisé avec succès",
                                Objet = user,
                            };
                            return reponse;
                        }
                        else
                        {
                            reponse = new ReponseAPI
                            {
                                StatusCode = 500,
                                Message = "erreur",
                            };
                            return reponse;
                        }
                    

                }
            }
            catch (Exception ex)
            {
                reponse = new ReponseAPI
                {
                    StatusCode = 500,
                    Message = IEnumError.GetByIdError + TYPE_DATA,
                    MessageException = ex.Message + ex.InnerException
                };
                return reponse;
            }


        }

        #endregion

    }
}
