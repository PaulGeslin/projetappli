﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;


namespace ApiFestival
{
    public class FestivalService : IService<Festival>
    {
        private readonly ApiFestivalContext _context;
        private const string TYPE_DATA = "Personne";
         
        public FestivalService(ApiFestivalContext context)
        {
            _context = context;
        }


        /// <summary>
        /// Récupérer une Personne à partir de son id 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ReponseAPI GetById(int id)
        {
            ReponseAPI reponse;

            try
            {
                Festival personne = _context.Festival.DefaultIfEmpty().FirstOrDefault(x => x.Id == id);

                reponse = new ReponseAPI
                {
                    StatusCode = 200,
                    Objet = personne
                };
                return reponse;
            }
            catch (Exception ex)
            {
                using (Logger logger = new Logger())
                {
                    logger.WriteLog(IEnumError.GetByIdError + TYPE_DATA, ex);
                }
                reponse = new ReponseAPI
                {
                    StatusCode = 500,
                    Message = IEnumError.GetByIdError + TYPE_DATA,
                    MessageException = ex.Message + ex.InnerException
                };
                return reponse;
            }
        }


        /// <summary>
        /// Ajouter une Personne
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ReponseAPI Add(Festival data)
        {
            ReponseAPI reponse;

            //Mauvaise donnée à insérer => générer une erreur 400
            if (data == null  )
            {
                reponse = new ReponseAPI
                {
                    StatusCode = 500,
                    Message = "Veuillez saisir les valeurs",
                    Objet = data
                };
                return reponse;

            }


            try
            {
                _context.Festival.Add(data);
                _context.SaveChanges();

                reponse = new ReponseAPI
                {
                    StatusCode = 200,
                    Objet = data
                };
                return reponse;
            }
            catch (Exception ex)
            {
                using (Logger logger = new Logger())
                {
                    logger.WriteLog(IEnumError.AddOneError + TYPE_DATA, ex);
                }
                reponse = new ReponseAPI
                {
                    StatusCode = 500,
                    Message = IEnumError.AddOneError + TYPE_DATA,
                    MessageException = ex.Message + ex.InnerException
                };
                return reponse;
            }

        }

        /// <summary>
        /// Modifier une Personne
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ReponseAPI Update(Festival data)
        {
            ReponseAPI reponse;
            Festival oldData;

            if (data == null )
            {
                reponse = new ReponseAPI
                {
                    StatusCode = 500,
                    Message = "Veuillez saisir les valeurs",
                    Objet = data
                };
                return reponse;
            }

            try
            {
                oldData = _context.Festival.DefaultIfEmpty().FirstOrDefault(x => x.Id == data.Id);
                if (oldData == null)
                {
                    reponse = new ReponseAPI
                    {
                        StatusCode = 400,
                        Message = IEnumError.GetByIdError + TYPE_DATA,
                        Objet = data
                    };
                    return reponse;
                }
            }
            catch (Exception ex)
            {
                using (Logger logger = new Logger())
                {
                    logger.WriteLog(IEnumError.GetByIdError + TYPE_DATA, ex);
                }
                reponse = new ReponseAPI
                {
                    StatusCode = 500,
                    Message = IEnumError.GetByIdError + TYPE_DATA,
                    MessageException = ex.Message + ex.InnerException
                };
                return reponse;
            }

            _context.Entry(oldData).State = EntityState.Detached;

            try
            {

                _context.Festival.Update(data);
                _context.SaveChanges();

                reponse = new ReponseAPI
                {
                    StatusCode = 200,
                    Objet = data
                };
                return reponse;
            }
            catch (Exception ex)
            {
                using (Logger logger = new Logger())
                {
                    logger.WriteLog(IEnumError.UpdateOneError + TYPE_DATA, ex);
                }
                reponse = new ReponseAPI
                {
                    StatusCode = 500,
                    Message = IEnumError.UpdateOneError + TYPE_DATA,
                    MessageException = ex.Message + ex.InnerException
                };
                return reponse;
            }
        }

        public ReponseAPI Synchro(List<Festival> liste)
        {
            throw new NotImplementedException();
        }

        public ReponseAPI GetAllArtistsByFestivalId(int id)
        {
           List<Artistes> artistes= _context.Artistes.Where(x => x.IdFestival == id).ToList();

            ReponseAPI reponse;

            reponse = new ReponseAPI
            {
                StatusCode = 200,
               
                Objet = artistes
            };
            return reponse;
        }
    }
}